# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=pulumi-language-yaml
pkgver=0.5.7
pkgrel=0
pkgdesc="Infrastructure as Code SDK (YAML language provider)"
url="https://pulumi.com/"
arch="all"
license="Apache-2.0"
depends="pulumi"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/pulumi/pulumi-yaml/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/pulumi-yaml-$pkgver"
# Requires PULUMI_ACCESS_TOKEN for tests to be run
options="!check"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -modcacherw"

build() {
	go build -v \
		-ldflags "-X github.com/pulumi/pulumi-yaml/pkg/version.Version=v$pkgver" \
		-o $pkgname \
		./cmd/pulumi-language-yaml/
}

package() {
	install -Dm755 $pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
e70d525911e2c526f53f7cf446837ecc34512842a8a49ee75e190178d3b6463eaf893d0d47f743d5dc99974b347906179d935161fa397ff8e88db08774309cf4  pulumi-language-yaml-0.5.7.tar.gz
"
